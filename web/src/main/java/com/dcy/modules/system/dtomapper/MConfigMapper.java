package com.dcy.modules.system.dtomapper;

import com.dcy.modules.system.dto.input.ConfigCreateInputDTO;
import com.dcy.modules.system.dto.output.ConfigListOutputDTO;
import com.dcy.modules.system.dto.input.ConfigSearchInputDTO;
import com.dcy.modules.system.dto.input.ConfigUpdateInputDTO;
import com.dcy.system.model.Config;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 15:03
 */
@Mapper(componentModel = "spring")
public interface MConfigMapper {

    Config configCreateInputDTOToConfig(ConfigCreateInputDTO configCreateInputDTO);

    Config configUpdateInputDTOToConfig(ConfigUpdateInputDTO configUpdateInputDTO);

    Config configSearchInputDTOToConfig(ConfigSearchInputDTO configSearchInputDTO);

    ConfigListOutputDTO configToConfigListOutputDTO(Config config);

    List<ConfigListOutputDTO> configsToConfigListOutputDTOs(List<Config> configs);
}
